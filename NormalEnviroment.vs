
attribute vec3 Position;
attribute vec3 Normal;
attribute vec3 Tangents;
attribute vec3 Bitangents;
attribute vec2 vTexcoord;

uniform mat4 matWorldViewProjection;
uniform mat4 matWorldIT;
uniform mat4 matWorld;

varying vec3 OutWorld;
varying vec2 OutTexcoord;
varying mat3 OutTBN;

void main()
{
    OutTexcoord = vTexcoord;
	
    vec3 oNormal = (matWorldIT * vec4(Normal, 0.0)).xyz;
	vec3 oTangent = (matWorldIT * vec4(Tangents, 0.0)).xyz;
	vec3 oBitangent = (matWorldIT * vec4(Bitangents, 0.0)).xyz;
	OutTBN = (mat3(oTangent, oBitangent, oNormal));
	
	OutWorld = (matWorld * vec4(Position, 1.0)).xyz;
    gl_Position = matWorldViewProjection * vec4(Position, 1.0);
}
