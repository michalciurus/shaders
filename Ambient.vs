
attribute vec3 Position;
attribute vec2 vTexcoord;

uniform mat4 matWorldViewProjection;

varying vec2 OutTexcoord;

void main()
{
    OutTexcoord = vTexcoord;
    gl_Position = matWorldViewProjection * vec4(Position, 1.0);
}