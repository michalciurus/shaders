
attribute vec3 Position;
attribute vec3 Normal;
attribute vec2 vTexcoord;

uniform mat4 matWorldViewProjection;
uniform mat4 matWorld;
uniform mat4 matWorldIT;

varying vec3 OutWorld;
varying vec2 OutTexcoord;
varying vec3 OutNormal;

void main(void)
{
    OutTexcoord = vTexcoord;
    OutNormal = (matWorldIT * vec4(Normal, 0.0)).xyz;
	OutWorld = (matWorld * vec4(Position, 1.0)).xyz;
    gl_Position = matWorldViewProjection * vec4(Position, 1.0);
}