
attribute vec3 Position;
attribute vec3 Normal;

uniform mat4 matWorldViewProjection;
uniform mat4 matWorld;
uniform mat4 matWorldIT;

uniform vec3 CamPos;

varying vec3 OutNormal;
varying vec2 OutTexcoord;

void main()
{
	vec4 Pos = vec4(Position, 1.0);
    OutNormal = normalize(( matWorldIT * vec4(Normal, 0.0) ).xyz);

	vec3 Eye = normalize(CamPos);
	float LdotN = dot(OutNormal, Eye);

	vec3 vReflect = 2.0 * LdotN * OutNormal - Eye;

	vReflect.xy = (vReflect.xy + 1.0) * 0.5;
	//vReflect.xy = 1.0 - vReflect.xy;
	OutTexcoord = vReflect.xy;
	
    gl_Position = matWorldViewProjection * Pos;
}